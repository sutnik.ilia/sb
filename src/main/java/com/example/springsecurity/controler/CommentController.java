package com.example.springsecurity.controler;

import com.example.springsecurity.app.TitleApplication;
import com.example.springsecurity.dto.CommentDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/comment")
public class CommentController {
    private final TitleApplication titleApplication;
    Logger logger = LoggerFactory.getLogger(CommentController.class);

    public CommentController(TitleApplication titleApplication) {
        this.titleApplication = titleApplication;
    }

    @GetMapping("/{id}")
    public String getComments(@PathVariable Long id, Model model) {
        model.addAttribute("listComments", titleApplication.listComments(id));

        logger.debug("Comment listed");

        return "commentsList";
    }

    @GetMapping("/get/{id}")
    public String GetComment(@PathVariable Long id, Model model) {
        logger.debug("GEt comment by id " + id);
        model.addAttribute("comment", titleApplication.getCommentById(id));
        return "comment";
    }

    @GetMapping("/create")
    public String getFromComments() {
        logger.debug("Was call form for creating comment");
        return "createComment";
    }

    @PostMapping
    public String createComment(@ModelAttribute CommentDto commentDto) {
        titleApplication.createComment(commentDto);
        logger.debug("Comment was creating" + commentDto.getDescription());
        return "redirect:/title";
    }

    @GetMapping("/edit/{id}")
    public String getEditFormComment(@PathVariable Long id, Model model) {
        logger.debug(String.valueOf(titleApplication.getCommentById(id)));
        System.out.println(titleApplication.getCommentById(id));
        model.addAttribute("comment", titleApplication.getCommentById(id));
        return "editComment";
    }


    @ExceptionHandler(value = Exception.class)
    public String exception(Exception e) {
        logger.error("Error in CommentController" + e.toString());
        logger.error("Details", e);
        return "redirect:/title";
    }

}
