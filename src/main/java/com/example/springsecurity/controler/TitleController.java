package com.example.springsecurity.controler;

import com.example.springsecurity.app.TitleApplication;
import com.example.springsecurity.dto.TitleDto;
import com.example.springsecurity.model.Title;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/title")
public class TitleController {

    private final TitleApplication titleApplication;

    Logger logger = LoggerFactory.getLogger(CommentController.class);

    public TitleController(TitleApplication titleApplication) {
        this.titleApplication = titleApplication;
    }

    @GetMapping
    public String getList(Model model) {
        model.addAttribute("list", titleApplication.list());
        logger.debug("Title listed");
        return "titleList";
    }

    @GetMapping("/{id}")
    public String getById(@PathVariable Long id, Model model) {
        model.addAttribute("title", titleApplication.getById(id));
        logger.debug("Was call title by id " + id);
        return "title";
    }

    @GetMapping("/create")
    public String getCreateForm() {
        logger.debug("Form for create title was called");
        return "createTitle";
    }

    @GetMapping("/edit/{id}")
    public String getEditForm(@PathVariable Long id, Model model) {
        logger.debug(String.valueOf(titleApplication.getById(id)));
        System.out.println(titleApplication.getById(id));
        model.addAttribute("title", titleApplication.getById(id));
        return "editTitle";
    }

    @PostMapping
    public String create(@ModelAttribute TitleDto titleDto) {
        titleApplication.create(titleDto);
        logger.debug("New title created" + titleDto.getName());
        return "redirect:/title";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute TitleDto titleDto) {
        titleApplication.update(titleDto);
        logger.debug("Title update");
        return "redirect:/title";
    }

    @PostMapping("/delete/{id}")
    public String delete(@PathVariable Long id) {
        titleApplication.deleteById(id);
        logger.debug("Title deleted!");
        return "redirect:/title";
    }
}
