package com.example.springsecurity.model;

import com.example.springsecurity.dto.TitleDto;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "title")
public class Title {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    public Title() {

    }

    public Title(TitleDto titleDto) {
     this.id = titleDto.getId();
     this.name = titleDto.getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String title) {
        this.name = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Title title = (Title) o;
        return Objects.equals(id, title.id) && Objects.equals(name, title.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
