package com.example.springsecurity.repository;

import com.example.springsecurity.model.Title;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

public interface TitleRepository extends Repository<Title, Long> {
    Title save(Title title);
    List<Title> findAll();
    Optional<Title> getById(Long id);
    void deleteById(Long id);
}
