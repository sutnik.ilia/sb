package com.example.springsecurity.repository;

import com.example.springsecurity.model.Comment;
import com.example.springsecurity.model.Title;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

public interface CommentRepository extends Repository<Comment, Long> {
    Comment save(Comment comment);
    List<Comment> findAllByTitleId(Long id);
    Optional<Comment> getById(Long id);
    void deleteById(Long id);
}
