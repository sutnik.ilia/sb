package com.example.springsecurity.app;

import com.example.springsecurity.controler.CommentController;
import com.example.springsecurity.dto.CommentDto;
import com.example.springsecurity.dto.TitleDto;
import com.example.springsecurity.model.Comment;
import com.example.springsecurity.model.Role;
import com.example.springsecurity.model.Title;
import com.example.springsecurity.model.User;
import com.example.springsecurity.repository.CommentRepository;
import com.example.springsecurity.repository.TitleRepository;
import com.example.springsecurity.repository.UserRepository;
import com.example.springsecurity.service.CommentMapper;
import com.example.springsecurity.service.TitleMapper;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Transactional
public class TitleApplication {

    private final TitleRepository titleRepository;
    private final TitleMapper titleMapper;
    private final CommentRepository commentRepository;
    private final CommentMapper commentMapper;



    public TitleApplication(TitleRepository titleRepository,
                            TitleMapper titleMapper,
                            CommentRepository commentRepository,
                            CommentMapper commentMapper) {
        this.titleRepository = titleRepository;
        this.titleMapper = titleMapper;
        this.commentRepository = commentRepository;
        this.commentMapper = commentMapper;
    }

    public Long create(TitleDto titleDto) {
       return titleRepository.save(new Title(titleDto)).getId();
    }

    public List<TitleDto> list() {
        return titleRepository.findAll()
                .stream()
                .map(titleMapper::mapToDto)
                .collect(Collectors.toList());
    }

    public TitleDto getById(Long id) {
        Title title = titleRepository.getById(id).get();
        return titleMapper.mapToDto(title);
    }

    public void update(TitleDto titleDto) {
        titleRepository.save(new Title(titleDto));
    }

    public void deleteById(Long id) {
        titleRepository.deleteById(id);
    }

    public Long createComment(CommentDto commentDto) {
        Comment comment = commentMapper.mapToEntity(commentDto);
        return commentRepository.save(comment).getId();
    }

    public List<CommentDto> listComments(Long titleId) {
       return commentRepository.findAllByTitleId(titleId)
                .stream()
                .map(commentMapper::mapToDto)
                .collect(Collectors.toList());
    }

    public CommentDto getCommentById(Long id) {
        Comment comment = commentRepository.getById(id).get();
        return commentMapper.mapToDto(comment);
    }
}
