package com.example.springsecurity.service;

import com.example.springsecurity.dto.CommentDto;
import com.example.springsecurity.model.Comment;
import com.example.springsecurity.model.Title;
import com.example.springsecurity.repository.TitleRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CommentMapper implements DtoMapper<CommentDto, Comment> {
        private final TitleRepository titleRepository;

    public CommentMapper(TitleRepository titleRepository) {
        this.titleRepository = titleRepository;
    }

    @Override
    public CommentDto mapToDto(Comment comment) {
        CommentDto commentDto = new CommentDto();
        commentDto.setId(comment.getId());
        commentDto.setDescription(comment.getDescription());
        commentDto.setTitleId(comment.getTitle().getId());
        return commentDto;
    }

    public Comment mapToEntity(CommentDto commentDto) {
        Comment comment = new Comment();
        comment.setId(commentDto.getId());
        comment.setDescription(commentDto.getDescription());
        Optional<Title> titleOptional = titleRepository.getById(commentDto.getTitleId());
        if (titleOptional.isPresent()) {
            comment.setTitle(titleOptional.get());
        } else {
            throw new RuntimeException("Can't find title");
        }
        return comment;
    }
}
