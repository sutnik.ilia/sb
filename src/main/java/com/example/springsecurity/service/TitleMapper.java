package com.example.springsecurity.service;

import com.example.springsecurity.dto.TitleDto;
import com.example.springsecurity.model.Title;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class TitleMapper implements DtoMapper<TitleDto, Title> {


    @Override
    public TitleDto mapToDto(Title title) {
        TitleDto titleDto = new TitleDto();
        titleDto.setName(title.getName());
        titleDto.setId(title.getId());
        return titleDto;
    }
}
