package com.example.springsecurity.service;

public interface DtoMapper<D, T>{
    D mapToDto(T t);
}
