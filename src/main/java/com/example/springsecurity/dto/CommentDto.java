package com.example.springsecurity.dto;


public class CommentDto {
    private Long id;
    private String description;
    private Long titleId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getTitleId() {
        return titleId;
    }

    public void setTitleId(Long titleId) {
        this.titleId = titleId;
    }

    @Override
    public String toString() {
        return "CommentDto{" +
                "id=" + id +
                ", description='" + description + '\'' +
                '}';
    }
}
